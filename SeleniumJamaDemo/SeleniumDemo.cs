﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumJamaDemo
{

    [TestFixture]
    public class SeleniumDemo
    {
        /* Demonstrating knowledge of member variables with proper access modifiers and proper notation */
        private IWebDriver driver;
        private string searchBoxXpath;
        private string searchResultsXpath;
        private int expectedCount;

        public string SearchUrl { get; protected set; }
        public string SearchParam { get; set; }
        

        [TestFixtureSetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
        }

        /* Normally this method would be just driver.Url = "http://www.google.com",
         * but I'm demonstrating usage of access modifiers in this code. */
        [SetUp]
        public void Setup()
        {
            this.SearchUrl = "http://www.google.com";
            this.SearchParam = "Jama Software";
            searchBoxXpath = "//*[@id=\"gbqfq\"]";
            searchResultsXpath = "//ol[@id=\"rso\"]/li";
            expectedCount = 1;
            driver.Navigate().GoToUrl(this.SearchUrl);
        }

        [Test]
        public void CountSearchResults()
        {
            int count = 0;

            var element = driver.FindElement(By.XPath(searchBoxXpath));
            element.SendKeys(this.SearchParam);

            // Wait until document is loaded with results and set count
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(5000));
            wait.Until(d => (count = driver.FindElements(By.XPath(searchResultsXpath)).Count) > 0);

            Assert.GreaterOrEqual(count, expectedCount);
        }
        
        [TestFixtureTearDown]
        public void TearDown()
        {
            if(driver != null)
                driver.Quit();
        }
    }
}
